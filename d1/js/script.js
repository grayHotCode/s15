
let faveFood = "Boiled Egg";
let sum = 150 + 9;
let product = 100 * 90;
let isActive = true;

// Group of Data
let restaurant = ['mcDo', 'Max', null , null, null];

// Object Literal
let faveArtist = {
	firstName: 'Megan Denise',
	lasName: 'Fox',
	stageName: 'Megan Fox',
	birthDay: '16 May 1986',
	age: 18,
	bestAlbum: null,
	bestSongs: null,
	isActive: true 
}

console.log(faveFood);
console.log(sum);
console.log(product);
console.log(isActive);
console.log(restaurant);
console.log(faveArtist);



// function
function divideNum(num1, num2){
	console.log(num1/num2);
	return num1/num2;
};

let quotient = divideNum(50, 10);

console.log(`The result of the: ${quotient}`);



let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

// Long Cut
num1 = num1+num4;
// Short Cut
num1 += num4;
console.log(num1);

// quotient(6,2);

// function quotient(num1, num2){
// 		div = num1 / num2;
// 		return div;
// }

// console.log("The result of the division is " + div);



let string1 = "Boston";
let string2 = " Celtics";

string1 += string2;

console.log(string1);







